import React, { useEffect } from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { useDispatch, useSelector } from "react-redux";
import { getPhotosAsync } from "../features/albums";
import AlbumItem from "../components/AlbumItem/AlbumItem";

const AllAlbumsView = () => {
  const dispatch = useDispatch();

  const albums = useSelector((state) => state.albums.value);

  useEffect(() => {
    dispatch(getPhotosAsync());
  }, [dispatch]);

  return (
    <Box sx={{ paddingTop: "100px", paddingBottom: "100px" }}>
      <Box sx={{ width: "80vw", margin: "0 auto" }}>
        <Typography variant="h5" sx={{ textAlign: "left" }}>
          Albums:
        </Typography>
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            gap: "10px",
            paddingTop: "50px",
          }}
        >
          {albums.map((album) => (
            <AlbumItem key={album[0].albumId} album={album} />
          ))}
        </Box>
      </Box>
    </Box>
  );
};

export default AllAlbumsView;
