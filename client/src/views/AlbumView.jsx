import React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import PhotoCard from "../components/PhotoCard/PhotoCard";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";

const AlbumView = () => {
  const { id } = useParams();
  const album = useSelector((state) => state.albums.value[+id - 1]);

  return (
    <Box style={{ paddingTop: "100px", paddingBottom: "100px" }}>
      <Box sx={{ width: "80vw", margin: "0 auto" }}>
        <Typography variant="h5" sx={{ textAlign: "left" }}>
          Album {id}
        </Typography>
        <Grid container spacing={{ xs: 2, md: 3 }} sx={{ paddingTop: "50px" }}>
          {album.map((photo) => (
            <Grid item xs={2} sm={4} md={4} key={photo.id}>
              <PhotoCard photo={photo} />
            </Grid>
          ))}
        </Grid>
      </Box>
    </Box>
  );
};

export default AlbumView;
