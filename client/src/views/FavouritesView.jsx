import React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import PhotoCard from "../components/PhotoCard/PhotoCard";
import { useSelector } from "react-redux";

const FavouritesView = () => {
  const favourites = useSelector((state) => state.favourites.value);

  if (!favourites.length) {
    return (
      <Box sx={{ paddingTop: "100px" }}>
        <Box sx={{ width: "80vw", margin: "0 auto" }}>
          <Typography variant="h5">
            Oh no! Looks like you have no favourites.
          </Typography>
        </Box>
      </Box>
    );
  }
  return (
    <Box sx={{ paddingTop: "100px", paddingBottom: "100px" }}>
      <Box sx={{ width: "80vw", margin: "0 auto" }}>
        <Typography variant="h5" sx={{ textAlign: "left" }}>
          Favourites
        </Typography>
        <Grid container spacing={{ xs: 2, md: 3 }} sx={{ paddingTop: "50px" }}>
          {favourites.map((photo) => (
            <Grid item xs={2} sm={4} md={4} key={photo.id}>
              <PhotoCard photo={photo} />
            </Grid>
          ))}
        </Grid>
      </Box>
    </Box>
  );
};

export default FavouritesView;
