import './App.css';
import Header from './components/Header/Header';
import { Routes, Route, Navigate} from 'react-router-dom'
import AllAlbumsView from './views/AllAlbumsView';
import AlbumView from './views/AlbumView';
import FavouritesView  from './views/FavouritesView';

const App = () => {

  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path='/' element={<Navigate to='/albums'/>}/>
        <Route path='/albums' element={<AllAlbumsView/>}/>
        <Route path='/albums/:id' element={<AlbumView/>}/>
        <Route path='/favourites' element={<FavouritesView/>}/>
      </Routes>
    </div>
  );
}

export default App;
