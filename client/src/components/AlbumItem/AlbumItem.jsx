import React, { useState } from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Checkbox from "@mui/material/Checkbox";
import { useNavigate } from "react-router-dom";

const AlbumItem = ({ album }) => {
  const [selected, setSelected] = useState(false);

  const navigate = useNavigate();

  const handleSelect = (id) => {
    setSelected(true);
    setTimeout(() => navigate(`/albums/${id}`), 50);
  };
  return (
    <Box>
      <img src={album[0].thumbnailUrl} alt={`album ${album[0].albumId}`}></img>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Typography>{`Album ${album[0].albumId}`}</Typography>
        <Checkbox
          checked={selected}
          onChange={() => handleSelect(album[0].albumId)}
        />
      </Box>
    </Box>
  );
};

export default AlbumItem;
