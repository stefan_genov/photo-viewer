import * as React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import Checkbox from "@mui/material/Checkbox";
import FavoriteBorder from "@mui/icons-material/FavoriteBorder";
import Favorite from "@mui/icons-material/Favorite";
import { useSelector, useDispatch } from "react-redux";
import { addFavourite, removeFavourite } from "../../features/favourites";

const PhotoCard = ({ photo }) => {
  const favourites = useSelector((state) => state.favourites.value);
  const isFavourite = favourites.some((favourite) => favourite.id === photo.id);
  const dispatch = useDispatch();

  const handleFavourite = () => {
    isFavourite
      ? dispatch(removeFavourite(photo))
      : dispatch(addFavourite(photo));
  };

  return (
    <Card sx={{ maxWidth: 345, height: 400, position: "relative" }}>
      <CardMedia
        component="img"
        height="200"
        image={photo.url}
        alt={`photo ${photo.id}`}
      />
      <CardContent>
        <Typography gutterBottom variant="h6" component="div">
          {photo.title}
        </Typography>
      </CardContent>
      <CardActions sx={{ position: "absolute", bottom: 0, right: 0 }}>
        <Checkbox
          checked={isFavourite}
          icon={<FavoriteBorder />}
          checkedIcon={<Favorite />}
          onChange={handleFavourite}
        />
      </CardActions>
    </Card>
  );
};

export default PhotoCard;
