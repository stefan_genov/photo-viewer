import React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import PhotoCameraIcon from "@mui/icons-material/PhotoCamera";
import { NavLink } from "react-router-dom";

const Header = () => {
  return (
    <Box sx={{ flexGrow: 1, alignItems: "center" }}>
      <AppBar position="static">
        <Toolbar sx={{ width: "80vw", margin: "0 auto" }}>
          <NavLink id="photo-icon" to="/">
            <PhotoCameraIcon sx={{ mr: 2, color: "white" }} />
          </NavLink>
          <Typography variant="h6" component="div">
            Photo Viewer
          </Typography>
          <Box sx={{ display: "flex", ml: 3, gap: 2 }}>
            <NavLink to="/albums" style={{ textDecoration: "none" }}>
              <Typography
                component="div"
                sx={{ fontSize: "1.2rem", color: "white" }}
              >
                Albums
              </Typography>
            </NavLink>
            <NavLink to="/favourites" style={{ textDecoration: "none" }}>
              <Typography
                component="div"
                sx={{ fontSize: "1.2rem", color: "white" }}
              >
                Favorites
              </Typography>
            </NavLink>
          </Box>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Header;
