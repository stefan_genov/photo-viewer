import { createSlice } from "@reduxjs/toolkit";

const favouritesSlice = createSlice({
  name: "favourites",
  initialState: { value: [] },
  reducers: {
    addFavourite: (state, action) => {
      state.value = [...state.value, action.payload];
    },
    removeFavourite: (state, action) => {
      state.value = state.value.filter(
        (favourite) => favourite.id !== action.payload.id
      );
    },
  },
});

export const { addFavourite, removeFavourite } = favouritesSlice.actions;

export default favouritesSlice.reducer;
