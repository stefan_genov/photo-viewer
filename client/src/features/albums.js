import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { API } from "../common/constants";

export const getPhotosAsync = createAsyncThunk(
  "albums/getPhotosAsync",
  async () => {
    const response = await fetch(`${API}`);

    if (response.ok) {
      const photos = response.json();

      return photos;
    }
  }
);

const albumsSlice = createSlice({
  name: "albums",
  initialState: { value: [] },
  reducers: {},
  extraReducers: {
    [getPhotosAsync.fulfilled]: (state, action) => {
      const albums = new Map();

      const albumIds = new Set(action.payload.map((photo) => photo.albumId));

      albumIds.forEach((id) => {
        const photos = action.payload.filter((photo) => photo.albumId === id);

        albums.set(id, photos);
      });

      state.value = Array.from(albums, ([key, value]) => value);
    },
  },
});

export default albumsSlice.reducer;
