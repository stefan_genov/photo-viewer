# Photo Viewer

A react app for viewing albums and photos.

## Getting started

- Clone the project
```bash
  git clone https://gitlab.com/stefan_genov/photo-viewer.git
```

- Go to the project directory
```bash
  cd client
```

- Client: Install dependencies and start
```bash
  npm install 
  npm start
```

## Features
- Browse albums
- Select and view specific album
- Add/remove photos to/from favourites
- View favourite photos


